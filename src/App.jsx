import React, { Component } from 'react';
import logo from './logo.svg';

import Header from './components/Header';
import Nav from './components/Nav';
import Content from './components/Content';
import Footer from './components/Footer';

import './App.css';



function App(){
  return (
    <div>
      <Header 
      title="Hola mundo" 
      subtitles="Ya se React" 
      />

      <Header title="Hola mamá"/>
      <Header title="Hola papá"/>
      <Nav />
      <Content />
      <Footer />
    </div>
  );  
}

export default App;
