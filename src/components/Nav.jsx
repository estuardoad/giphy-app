import React from 'react';

const NavList = [
    {
        title: 'Link 1',
        url: '/link-1',
    },
    {
        title: 'Link 2',
        url: '/link-2',
    },
    {
        title: 'Link 3',
        url: '/link-3',
    },
];


function Nav(){
    return (
      <ul>
        {
            NavList.map(elemento => (
            <li>
                <a href={elemento.url}>
                    {elemento.title}
                </a>
            </li>
            ))
        }
      </ul>
    );
  }

  export default Nav;