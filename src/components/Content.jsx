import React from 'react';

function Content(){
    return (
      <div>
        <p>Lorem ipsum dolor sit amet consectetur adipisicing elit. Sunt debitis est non magnam facilis? Neque blanditiis praesentium iste, nostrum optio perspiciatis nam sit doloribus laboriosam, enim maxime dicta minus molestiae.
        </p>
        <p>Lorem, ipsum dolor sit amet consectetur adipisicing elit. Aut, dolor culpa quibusdam molestiae ea aliquam consectetur soluta. Molestias, repudiandae esse voluptas quis cum ducimus nam nobis modi, numquam eum consectetur?</p>
        <p>Lorem ipsum dolor sit amet consectetur adipisicing elit. Sed, accusamus eum vero harum ab fuga sunt saepe porro est ea odio consequatur dolorem magni sapiente? Voluptas nihil amet quaerat impedit.</p>
      </div>
    );
  }

  export default Content;